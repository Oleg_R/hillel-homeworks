'use strict'
// 1. Пользователь вводит в prompt() числа через запятую (один раз много цифр, например: "54, 45, 53453, 7, 32").
// - Разрешите пользователю вводить минимум три числа, максимум - десять.
// - Разрешите вводить только числа меньше ста.
// - Если всё введено верно, отсортируйте числа от меньшего к большему и выведите в консоль новую строку.
// * Усложнение (необязательно к выполнению): Отсортируйте числа на сначала чётные, потом нечётные.



// Решение
var numbers = [];
//Функция для сбора чисел в  масив   numbers
function collectNumbers() {
    var a = prompt ("Введите через запятую минимум три числа, максимум - десять чисел которые меньше чем число '100'. (Пример: 54, 45, 34, 7, 32).");
    if (a == null || a == "") {                    // Проверка на отмену ввода
        return alert ('Спасибо!');
    } 
    //Преобразует строку в масив       
    a = a.split(', ');  
    //Преобразует строковые значения в числовые.  Синтаксис ES6, можно заменить 
    //на ES5 numbers = a.map(function(elem) { return +elem;});                                    
    numbers = a.map(elem => +elem);   
    //Проверки на неправильный ввод      
    if (numbers.some(isNaN)) {
        alert ("Вы некоректно ввели числа");
        return collectNumbers();
    } else if (numbers.length < 3 || numbers.length > 10 ) {
        alert ("Вы ввели неправильное количество чисел");
        return collectNumbers();
    } else if (numbers.some(findMaximum)) {
        alert ("Вы ввели число которое больше или равно числу '100'");
        return collectNumbers();
    } else {  
        numbers.sort(compareNumeric);
        console.log ('Ваши числа отсортированые от меньшего к большему: ' + numbers.join(', ')); 
        findEvenAndOdd ();
    }
}
collectNumbers();
//функция для поиска чисел которые больше 100
function findMaximum(number) {
    return number >= 100;
}

function compareNumeric(a, b) {
    if (a > b) return 1;
    if (a < b) return -1;
}

//Усложнение, дополнительное задание 
function findEvenAndOdd () {
    //поиск четных
    var evenArr = numbers.filter(function(even) {
            return even%2 === 0;
        });  
    evenArr.sort(compareNumeric);
    //поиск нечетных
    var oddArr = numbers.filter(function(odd) {
            return odd%2 !== 0;
        });  
    oddArr.sort(compareNumeric);  
    console.log ('Четные числа отсортированые от меньшего к большему: ' + evenArr.join(', '));        
    console.log ('Нечетные числа отсортированые от меньшего к большему: ' + oddArr.join(', '));
}    
     

