'use strict'
// 2. Есть массив [3,, 6,,, 7,, undefined, 34,,,, 97,, 43].
// - Пользователь вводит индекс. Ответьте есть ли элемент c таким индексом и,
// если есть, выведите его в консоль. Так же перенесите его (элемент) в конец массива.

var arr = [3, undefined, 6, "red", 3,, 6,,, 7,, undefined, 34,,,, 97,, 43, "apple", 7, 9, undefined, 34, "call", 45, undefined,,,, "legend", 97,,, "strict", 43, 3, undefined,,,, 6, "red", "apple", 7, 9, undefined, 34,,,, "call", undefined, "legend", 97, "strict", 43];
var index = prompt ("Введите индекс в формате числа (Пример: 15)", 0);
if (index === null || index === "") {                    // Проверки на отмену ввода, пустую строку или не коректное значение
    alert ('Вы ничего не ввели. Спасибо!');
} else {
    index = +index;
    if (isNaN(index)) {
        alert ("Вы некоректно ввели индекс");       
    } 
}
//Само решение
if (index in arr) {
    console.log ("Такой индекс есть и в нем содержится значение: " + arr[index]);  
    var removed = arr.splice(index, 1);
    arr = arr.concat(removed);
} else {
    console.log ("Нет такого индекса");
}
  