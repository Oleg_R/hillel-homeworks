'use strict'
// 3. Напишите функцию, которая принимает название товара, его цену и категорию, добавляет этот товар в 
//массив, где каждый товар это объект и возвращает массив всех товаров. Товаров может быть сколько угодно.
//     [
//     {
//     name: 'pen',
//     price: 2,
//     category: pens
//     }
//     ]
// - Напишите функцию, которая фильтрует товары по цене от и до и возращает новый массив
//     только с товарами выбранного ценового диапазона или пустой массив.
// - Напишите функцию, которая фильтрует товары по категории и возращает новый массив только
//     с товарами выбранной категории, если она есть или пустой массив.
// - Напишите функцию, которая возвращает количесто товаров в категории.
// - Напишите функцию, которая удаляет товар по имени.
// - Напишите функции, которые сортируют товары по цене от меньшего к 
//     большему и наоборот и возвращают новый массив.
// - Напишите функцию, которая принимает вид сортировки (от большего к 
//     меньшему или наоборот) и фильтра (диапазон цены или категория) 
//     и возвращает новый массив товаров определённой выборки, отсортированные
//     как указал пользователь.
// - Напишите функцию, которая принимает вид сортировки (от большего к меньшему 
//     или наоборот) и фильтра (диапазон цены или категория) и возвращает сумму цен товаров этой выборки.
//     В этом задании переиспользуйте свои функции, где можно, что бы не дублировать функционал.
//     В итоге, пользователь может добавлять, удалять, сортировать, считать 
//     количество, фильтровать и считать сумму товаров.

//Решение
var productsList = [];

function collectionProduct () {
    var result = confirm("Вы желаете внести новый товар?");
    if (result) {
        var newProduct = {
            name: prompt ("Введите название товара"),
            price: prompt ("Введите цену товара"),
            category: prompt ("Введите категорию товара")
        };
        productsList.push(newProduct); 
        return collectionProduct ();
    } else {
        return productsList;
    }    
}

//Фильтр по цене (Цикл можно заменить на метод forEach как в следующем примере)
var filteredPrice;
function filterPrice () {
    filteredPrice = [];
    var min = prompt ("Введите минимальную цену товара");
    var max = prompt ("Введите максимальную цену товара");
    var product;     
    for (var key in productsList) {  
        product = productsList[key];    
        if (product['price'] >= min && product['price'] <= max) {
            filteredPrice.push(product);
        }        
    }
    return filteredPrice;
}

//Фильтр по категории
var filteredCategory;
function filterСategory () {
    filteredCategory = [];
    var n = prompt ("Введите нужную вам категорию");       
    productsList.forEach(function(item, i, arr) { 
        if (item['category'] == n) {
            filteredCategory.push(item);                        
        }                   
    }); 
    return filteredCategory;       
}

//Фильтр по категории и вывод количества товаров в даной категории
function amountProducts () {
    filterСategory ()
    console.log ("Количество товаров выбраной катигории - " + filteredCategory.length );
}


//Удаление товара по его названию
function deleteProduct () {
    var n = prompt ("Введите название товара который желаете удалить");   
    productsList.forEach(function(item, i, arr) {    
        if (item['name'] == n) {
            productsList.splice(i, 1);
        }    
    });    
}

//Фильтр товаров по цене от меньшего к большему и наоборот
function max(a, b) {
    if (a['price'] > b['price']) return 1;
    if (a['price'] < b['price']) return -1;
} 
function min(a, b) {
    if (a['price'] < b['price']) return 1;
    if (a['price'] > b['price']) return -1;
}

//Функция, которая принимает вид сортировки (от большего к меньшему или наоборот)
var fromSmallerToBigger = true;
var fromBiggerToSmaller = false;

function sort (toggle) { 
    if (toggle === true) {
        var productsListMax = productsList.slice(0).sort(max);
        console.log (productsListMax);
    } else if (toggle === false) {
        var productsListMin = productsList.slice(0).sort(min);
        console.log (productsListMin);
    }
}
sort (fromSmallerToBigger)  //Товары от меньшего к большему
sort (fromBiggerToSmaller)  //Товары от большего к меньшему


//Фильтр товаров по цене или категории от меньшего к большему или наоборот
function filterAll (a, b, c) { 
    var filteredNewList;
    if (a && b) {
        filterСategory (); 
        filterPrice ();
        var filterAll = filteredPrice.filter(function(obj) { 
            return filteredCategory.indexOf(obj) >= 0; 
        });
        if (c) {
            filteredNewList = filterAll.slice(0).sort(max);
            console.log(filteredNewList);
        } else {
            filteredNewList = filterAll.slice(0).sort(min);
            console.log(filteredNewList);
        } 
    } else {
        if (a) {
            filterСategory ();
            if (c) {
                filteredNewList = filteredCategory.slice(0).sort(max);
                console.log(filteredNewList);
            }  else {
                filteredNewList = filteredCategory.slice(0).sort(min);
                console.log(filteredNewList);
            }
        }
        if (b) {
            filterPrice ();
            if (c) {
                filteredNewList = filteredPrice.slice(0).sort(max);
                console.log(filteredNewList);
            } else {
                filteredNewList = filteredPrice.slice(0).sort(min);
                console.log(filteredNewList);
            } 
        }  
    }  
} 
filterAll (true, false, true);  //Товары по категории, от меньшего к большему
filterAll (true, false, false);  //Товары по категории, от большего к меньшему
filterAll (false, true, true);  //Товары по цене, от меньшего к большему
filterAll (false, true, false);  //Товары по цене, от большего к меньшему
filterAll (true, true, true);  //Товары по цене и категории, от меньшего к большему
filterAll (true, true, false);  //Товары по цене и категории, от большего к меньшему


function findAllSum (a, b) {
    var price = 0; 
    if (a && b) {
        filterСategory (); 
        filterPrice ();
        var filteredAll = filteredPrice.filter(function(obj) { return filteredCategory.indexOf(obj) >= 0; });
        var price = 0;
        function findPricesInAll () {
            var clone = {};    
            for (var key in filteredAll) {
                clone = filteredAll[key];
                price += clone.price;        
            }
            console.log ('Сумма всех товаров по катерегии в промежутке цен: ' + price);        
        };
        findPricesInAll ();
    } else {
        if (a) {
            filterСategory ();
            var price = 0;
            function findPricesInСategory () {
                var clone = {};    
                for (var key in filteredCategory) {
                    clone = filteredCategory[key];
                    price += clone.price;        
                }
                console.log ('Сумма всех товаров по катерегии: ' + price);        
            };
            findPricesInСategory ();                     
        }
        if (b) {
            filterPrice ();
            var price = 0;
            function findPricesInPrice () {
                var clone = {};    
                for (var key in filteredPrice) {
                    clone = filteredPrice[key];
                    price += clone.price;        
                }
                console.log ('Сумма всех товаров в промежутке цен: ' + price);        
            };
            findPricesInPrice ();            
        }  
    }  
}    
findAllSum (true, true); //Сумма всех по катерегии в промежутке цен
findAllSum (true, false); //Сумма всех по катерегии
findAllSum (false, true); //Сумма всех в промежутке цен


// Масив для тестирования функций
productsList = [
    {
        name: 'Яблоки',
        price: 10,
        category: 'фрукты'
    },
    {
        name: 'Карандаш',
        price: 12,
        category: 'канцтовары'
    },
    {
        name: 'Груши',
        price: 41,
        category: 'фрукты'
    },
    {
        name: 'Вазон',
        price: 32,
        category: 'декор'
    },
    {
        name: 'Ластик',
        price: 1,
        category: 'канцтовары'
    },
    {
        name: 'Лампа',
        price: 6,
        category: 'декор'
    },
    {
        name: 'Конфеты',
        price: 22,
        category: 'сладости'
    },
    {
        name: 'Клей',
        price: 11,
        category: 'канцтовары'
    },
    {
        name: 'Сливы',
        price: 63,
        category: 'фрукты'
    }
]