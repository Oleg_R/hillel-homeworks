'use strict'
// Практические задания:
// 3. Пользователь вводит в prompt() поочерёдно индекс (от 0 до 10, требуется проверка ввода),
//  название товара, его цену и категорию. Напишите функцию для сбора информации о товарах 
//  в объект, где свойствами выступают индексы товаров, а значениями объект с именем, ценой 
//  и категорией.
// {
// 0: {
// name: 'pen',
// price: 2,
// category: pens
// }
// }
// Напишите функцию, которая фильтрует товары по категории и возращает новый объект 
// только с товарами выбранной категории, известно, что в объекте их не больше десяти.
// Напишите функцию, которая выводит в консоль суму цен всех товаров определённой категории.
// Если сумма превышает 5, то выводится сумма со скидкой в 12% и пользователя получает 
// уведомление об этом.

//Функция для сбора информации о товарах в объект
var products = {};
function collectProducts () {    
    for (var i = 0; i <= 11; i++) {
        var index = prompt ("Введите индекс товара от 0 до 10");                
        if (+index > 10 || +index < 0 || isNaN(index) || index === "") {
            alert ('Вы ввели не верный индекс');
            return collectProducts ();
        } else if (index === null ) {
            i = 11;
            return alert ('Cпасибо');            
        } else if (products[index] !== undefined) {
            alert ('Товар с таким индексом уже существует');
            return collectProducts ();            
        } else {
            products[index] =  {
                name: prompt ("Введите название товара"),
                price: prompt ("Введите цену товара"),
                category: prompt ("Введите категорию товара")
            } 
        }         
    }
} 
collectProducts ();




//Функция, которая фильтрует товары по категории и возращает новый объект 
// только с товарами выбранной категории
var сategoryProducts = {};

function showСategory (a) {
    var product = {};    
    for (var key in products) {
        product = products[key];
        if (product.category == a) {
            сategoryProducts[key] = products[key];
        } 
    }
    return сategoryProducts;
}

//Функция, которая выводит в консоль суму цен всех товаров определённой категории.
//Если сумма превышает 5, то выводится сумма со скидкой в 12% и пользователя получает 
//уведомление об этом.
var price = 0;
function showPrices () {
    showСategory (prompt ("Введите категорию товаров сумму которых Вы хотите увидеть"));
    var product = {};    
    for (var key in сategoryProducts) {
        product = сategoryProducts[key];
        price = price + +product.price;
    }
    if (price > 5) {
        var discount = price - price * 0.12;
        console.log (discount);
        alert ("Вы получите данные товары со скидкой 12% и их сумма составит " + discount)
    } else {
        console.log (price);
    }    
}
showPrices ();