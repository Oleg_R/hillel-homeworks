'use strict'
// Практические задания:
// 2. Дана функция:
// function f (a, b, c) {
// function sum (a, b) {
// retur a + b;
// }
// }
// Перепишите её следующим образом:
// 2.1. Если агрументы a и b не переданы, они равны по умолчанию 2 и 3 соответсвенно.
// 2.2. Если аргумент c передан и он является функцией, то он выполняется после вызова функции sum.
// 2.4. Функция f должны возвращать результат функции аргумента c, если он есть, либо результат функции sum.

function f(a, b, c) { 
    if (a == undefined) {
        a = 2;
    }
    if (b == undefined) {
        b = 3;
    }
    function sum () {
        return (a + b);       
    }      
    if (c === undefined || typeof c != "function") {          
        return sum();                     
    } else if (typeof c == "function") {
        sum();
        return c();
    }
}