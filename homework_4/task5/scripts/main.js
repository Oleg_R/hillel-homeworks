"use strict";

// 5. Курс обмена валюты фиксированный и составляет 2772.4 украинских гривен за 100 долларов США.
// Выведите в консоль, сколько гривен положительной или отрицательной разницы в гривне получит
// пользователь после ввода суммы и нового курса.

var oldCourse = 2772.4 / 100;
var amountOfMoney = prompt("Введите сумму денег которую Вы бы хотели обменять");
var newСourse = prompt("Введите Новый курс доллара (Количество украинских гривен за 100 долларов США) (Пример: 2783,1)");

var oldSum;
var newSum;
var difference;

oldSum = oldCourse * amountOfMoney;
newSum = newСourse / 100 * amountOfMoney;

if (oldSum > newSum) {
    difference = oldSum - newSum;
    alert ('Вы потеряете ' + difference.toFixed(2) + ' гривен');
} else if (oldSum < newSum) {
    difference =  newSum - oldSum;
    alert ('Вы заработаете ' + difference.toFixed(2) + ' гривен');
} else if (oldSum == newSum) {
    alert ('Вы ничего не заработаете и не потеряете!');
}
