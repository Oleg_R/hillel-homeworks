"use strict";
// 3. Пользователь вводит три числа и знак + или -. Верните в консоль большее/меньшее,
//  в зависимости от введённого знака. Проверяйте все вводы пользователя.

var numberOne = prompt("Введите первое число!");
var numberTwo = prompt("Введите второе число!");
var numberThree = prompt("Введите третье число!");
var operators = prompt("Введите знак (+) или (-) чтобы получить бельшее или меньшее число!");

if (numberOne == null 
    || numberTwo == null 
    || numberThree == null 
    || operators == null){     //проверка на отмену ввода
    alert ('Вы не ввели одно или несколько значений!');         
} else if (isNaN(+numberOne) 
    || isNaN(+numberTwo) 
    || isNaN(+numberThree)){ //проверка на строку 
    alert ('Вы ввели не числа');    
} else if (operators != '-' && operators != '+') { //проверка на оператор
    alert ('Вы ввели неверный оператор');
} else {
    if (numberOne == numberTwo && numberTwo == numberThree) {
        alert ('Все числа одинаковые!');
    } else if (operators == '+') {
        if (numberOne >= numberTwo && numberOne > numberThree) {
            alert ('Число ' + numberOne + ' является самым большим!');
        } else if (numberTwo >= numberThree && numberTwo > numberOne) {
            alert ('Число ' + numberTwo + ' является самым большим!');
        } else if (numberThree >= numberOne && numberThree > numberTwo) {
            alert ('Число ' + numberThree + ' является самым большим!');
        }
    } else if (operators == '-') {
        if (numberOne <= numberTwo && numberOne < numberThree) {
            alert ('Число ' + numberOne + ' является самым маленьким!');
        } else if (numberTwo <= numberThree && numberTwo < numberOne) {
            alert ('Число ' + numberTwo + ' является самым маленьким!');
        } else if (numberThree <= numberOne && numberThree < numberTwo) {
            alert ('Число ' + numberThree + ' является самым маленьким!');
        }
    }
}    

