"use strict";
// 4. Пользователь по очереди вводит в prompt() названия фруктов по одному. Выведите в 
// консоль список всех фруктов, когда пользователь отменит ввод.

var fruitList = '';
var newFruit = true;
function collectFruit() {    
    while (newFruit) {
        newFruit = prompt("Введите название фрукта!");
        if (newFruit == null || newFruit == "") {  
            if (fruitList == "") {
                return console.log (fruitList);
            } else {
                return console.log (fruitList + '.');
            } 
        } else {
            if (fruitList == "") {
                fruitList += 'Список Ваших фруктов: ' + newFruit;
            } else {
                fruitList += ", " + newFruit;
            }
        }   
    } 
    return console.log (fruitList + '.');
}
collectFruit();