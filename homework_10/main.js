'use strict'
//В переменную "parts"  сохранены все части учебника
var parts = document.querySelectorAll('.list');
//Функция для поиска и вывода тем
function chapterSearch (part, chapter, style) {
    //Сохранение необходимой части учебника в переменную
    var selectedPart = parts[part - 1];
    //Сохранение главы учебника в переменную
    var selectedChapter = selectedPart.children[chapter - 1];    
    //Создание объекта с функциями вывода в разных стилях 
    var styles = {
        current: function () {
            var p = 1;
            //Переменная "topic" хранит лист (список) с темами
            var topic = selectedChapter.children[1].children;
            for (var i = 0; i < topic.length; i++) {
                console.log (chapter + "." + p++ + " " + topic[i].innerText);
            }
        },
        number: function () {
            var p = 1;
            var topic = selectedChapter.children[1].children;
            for (var i = 0; i < topic.length; i++) {
                console.log (p++ + " " + topic[i].innerText);
            }
        }, 
        dash: function () {
            var topic = selectedChapter.children[1].children;
            for (var i = 0; i < topic.length; i++) {
                console.log ("- " + topic[i].innerText);
            }
        }       
    }
    //Конструкция switch для выбора стиля вывода (по умолчанию "current" - нумерация, как в учебнике с сохранинием порядка)
    switch (style) {
        case 'current':
            styles.current();
            break;
        case 'number':
            styles.number();
            break;
        case 'dash':
            styles.dash();
            break;
        default:
            styles.current();
            break;        
    }    
}   
chapterSearch(2, 4, 'dash'); //для примера