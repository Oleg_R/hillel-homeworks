"use strict";

// В Одессе открыли наземное метро и ввели тарифы на перевозку ручной клади.
// Выделили три категории оплаты по массе:
// 1. 0 - 5 кг - 3 грн за килограмм.
// 2. 5 -10 кг - 5 грн за килограмм.
// 3. 10 -15 кг - 10 грн за килограмм.
// Через три дня приехал турист с Англии. Выведите в консоль сколько он
// должен заплатить фунтов стерлингов за свою кладь, массу которой он
//  вводит в prompt(), с учётом, что он вводит её в фунтах. 
// Курс валюты на момент ввода фиксированный и составляет 2.88 
// фунтов стерлингов за 100 украинских гривен.
// 1 кг = 2,20462 фунтов
// Учтите, что он джентельмен и может ввести массу прописью.
// Уведомьте его о том, что можно вводить только числа.

// Объявление переменных
var luggageWeightLb = 0;
var luggageWeightKg = 0;
var priceInHryvnia = 0;
var pricepriceInPounds = 0;
var sumInPounds = 0;
var sumInHryvnia = 0;
var poundRate = 0.0288;

function showAmount () {
    //Просим пользователя ввести вес
    luggageWeightLb = prompt('Please enter your baggage weight in pounds, in numbers. (Example: 15)',);

    if (luggageWeightLb == null ){     //проверка на отмену ввода
        alert ('Please enter your baggage weight'); 
        showAmount ();    
    } else if (isNaN(+luggageWeightLb)){ //проверк на строку 
        alert ('Please enter your baggage weight only in numbers. (Example: 15)');
        showAmount ();
    } else {
        luggageWeightLb = +luggageWeightLb
    }
    if (luggageWeightLb >= 0) {    
        //Переводим вес из фунтов в килограммы 
        var luggageWeightKg = luggageWeightLb / 2.20462;
        //Производим расчет цен по весу 
        if (luggageWeightKg > 0 && luggageWeightKg <= 5) {
            priceInHryvnia = 3;
            sumInPounds = priceInHryvnia * luggageWeightKg.toFixed(1) * poundRate;
        }
        if (luggageWeightKg > 5 && luggageWeightKg <= 10) {
            priceInHryvnia = 5;
            sumInPounds = priceInHryvnia * luggageWeightKg.toFixed(1) * poundRate;
        }
        if (luggageWeightKg > 10 && luggageWeightKg <= 15) {
            priceInHryvnia = 10;
            sumInPounds = priceInHryvnia * luggageWeightKg.toFixed(1) * poundRate;
        }
        if (luggageWeightKg > 15) {
            alert ('You have exceeded your maximum baggage weight!')
        } 
        //Выводим сколько необходимо оплатить
        alert ('You need to pay ' + sumInPounds.toFixed(2) + ' pounds. Thank you!')
    } 
}
showAmount ();